import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobseekersRoutingModule } from './jobseekers-routing.module';
import { ListingComponent } from './components/listing/listing.component';
import { MatModuleModule } from '../../mat-module.module';

@NgModule({
  imports: [
    CommonModule,
    JobseekersRoutingModule,
    MatModuleModule,    
  ],
  declarations: [ListingComponent]
})
export class JobseekersModule { }
