import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { CommonService } from '../../../../shared/services/common.service';
import { NotificationService } from '../../../../shared/services/notification.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})

export class ListingComponent implements OnInit {

  constructor(
    private notificationService: NotificationService,
    private commonService: CommonService,
    private http: HttpClient
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['name', 'email', 'mobile', 'action'];
  dataSource;
  singleData = {
    profileImage: '',
    isVerified: '',
    firstName: '',
    lastName: '',
    mobile: '',
    address: [],
    createdOn: '',
    email: '',
    isBlocked: ''
  };
  envtmnt = environment;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  toggleBlocking(data){
    console.log('status ', data);
    this.commonService.post('admin/users/toggleBlocking', data)
      .subscribe(response => {
        console.log(response);
        if(response && response['message']){
          this.dataSource.filteredData.forEach(elem => {
            if(elem._id == data._id)
              elem.isBlocked = data.isBlocked;
          });
          this.notificationService.toggleSnackBar('Status changed Successfully');
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
  }

  getSingleUserData(empId){
    this.dataSource.filteredData.forEach(elem => {
      if(elem._id == empId)
        this.singleData = elem;
    });
  }

  ngOnInit() {
    
    this.http.get(environment.baseUrl + 'admin/users')
      .subscribe((response: any) => {
        console.log('userList ', response)
        this.dataSource  =  new MatTableDataSource(response.response);
        this.dataSource['paginator'] = this.paginator;
        this.dataSource.sort = this.sort;
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      })

  }

}
