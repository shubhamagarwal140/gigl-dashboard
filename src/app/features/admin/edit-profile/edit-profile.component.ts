import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../shared/services/common.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { environment } from '../../../../environments/environment';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  constructor(
    private commonService: CommonService,
    private notificationService: NotificationService,
    public loctn: Location
  ) { }
  
  adminId;
  environment = environment;
  adminData = {
    firstName: '',
    lastName: '',
    email: '',
    image: '',
    phone: '',
    address: '',
    location: ''
  };

  updateAdminProfile(){
    var fd = new FormData();
    fd.append('email', this.adminData.email);
    fd.append('phone', this.adminData.phone);
    fd.append('address', this.adminData.address);
    fd.append('location', this.adminData.location);
    fd.append('image', this.adminData.image );
    console.log('updateAdminProfile ', fd)
    this.commonService.put(`admin/profile/${this.adminId}`, fd)
      .subscribe(response => {
        console.log(response);
        if(response && response['response'])
          this.notificationService.toggleSnackBar('Profile Updated Successfully');
        else
          this.notificationService.toggleSnackBar();
      }, error => {
        console.log('error ', error);
        this.notificationService.toggleSnackBar();
      } )
  }

  getAdminData(adminId){
    this.commonService.get(`admin/profile/${adminId}`)
      .subscribe(response => {
        console.log(response);
        this.adminData = response['response'];
      })
  }

  filee(file){
    var Ffile = file.target.files[0];
    var fileType = Ffile.type.split('/')[1];    
    if(fileType == 'png' || fileType == 'jpg' || fileType == 'jpeg')
      this.adminData.image = Ffile;
    else this.notificationService.toggleSnackBar('Only jpeg | jpg | png formats are allowed');
  }

  ngOnInit() {
    this.adminId = this.commonService.decodeToken._id;
    if(this.adminId)
      this.getAdminData(this.adminId)
    else
      this.notificationService.toggleSnackBar();
  }

}
