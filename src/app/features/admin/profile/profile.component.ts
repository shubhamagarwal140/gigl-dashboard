import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../shared/services/common.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private commonService: CommonService,
    private notificationService: NotificationService
  ) { }
  
  adminId;
  adminData;
  newpass;
  errMessage = '';
  environment = environment;

  getAdminData(adminId){
    this.commonService.get(`admin/profile/${adminId}`)
      .subscribe(response => {
        console.log(response);
        this.adminData = response['response'];
      })
  }

  matchPassword(pass){
    if(this.newpass !== pass)
      this.errMessage = 'Password does not match';
    else this.errMessage = '';
    return true;
  }

  changePassword(data){
    console.log('changePassword ', data)
    if(this.adminData._id){
      var putData = {
        password: data.newPass,
        oldPass: data.oldPass,
        _id: this.adminData._id
      }
      this.commonService.put('admin/changePass', putData)
        .subscribe(response => {
          console.log('changePassword ', response);
          if(response && response['message'])
            this.errMessage = response['message'];
        }, error => {
          this.errMessage = error['message'];          
        })
    }
    else
      this.notificationService.toggleSnackBar();
  }

  ngOnInit() {
    this.adminId = this.commonService.decodeToken._id;
    if(this.adminId)
      this.getAdminData(this.adminId)
    else
      this.notificationService.toggleSnackBar();
  }

}
