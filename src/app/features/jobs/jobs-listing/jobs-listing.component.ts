import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import {FormControl} from '@angular/forms';

import { environment } from '../../../../environments/environment';
import { CommonService } from '../../../shared/services/common.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-jobs-listing',
  templateUrl: './jobs-listing.component.html',
  styleUrls: ['./jobs-listing.component.scss']
})
export class JobsListingComponent implements OnInit {

  
  constructor(
    private http: HttpClient,
    private commonService: CommonService,
    private notificationService: NotificationService,
    public dialog: MatDialog,
    private router: ActivatedRoute
  ) { }
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['title', 'postedBy', 'pdate', 'mdate', 'status', 'action'];
  dataSource;
  singleData: any = {};
  envtmnt = environment;
  filterQuery: any = {};
  filterData = {
    status: [
      { title: 'Pending', value: 1 },
      { title: 'Active', value: 2 },
      { title: 'Finished', value: 3 }
    ],
    timing: [
      { title: '1hour', value: 3600000 },
      { title: '24hours', value: 86400000 },
      { title: '7days', value: 604800000 },
      { title: '30days', value: 2592000000 },
      { title: '12months', value: 31556952000 }
    ]
  }

  filterJobs(){
    var query = '?';
    if(this.filterQuery.status)
      query += `status=${this.filterQuery.status}&`;
    if(this.filterQuery.modifiedOn)
      query += `modifiedOn=${this.filterQuery.modifiedOn}`;
    console.log(query);
    this.http.get(environment.baseUrl + 'admin/jobs' + query)
    .subscribe((response: any) => {
      console.log('response ', response.response);
      this.dataSource  =  new MatTableDataSource(response.response); 
    });
    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  toggleBlocking(data){
    this.commonService.post('admin/jobs/toggleBlocking', data)
      .subscribe(response => {
        console.log(response);
        if(response && response['message']){
          this.dataSource.filteredData.forEach(elem => {
            if(elem._id == data._id)
              elem.isBlocked = data.isBlocked;
          });

          this.notificationService.toggleSnackBar('Status changed Successfully');
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
  }

  getSingleUserData(empId){
    this.dataSource.filteredData.forEach(elem => {
      if(elem._id == empId)
        this.singleData = elem;
    });
  }

  ngOnInit() {

    this.router.paramMap
    .subscribe(ro => {
      var empId = '';
      if(ro.get('empId'))
        empId =  ro.get('empId');
      console.log('empId ', empId);

      this.http.get(`${environment.baseUrl}admin/jobs?empId=${empId}`)
        .subscribe((response: any) => {
          console.log('response ', response.response);
          this.dataSource  =  new MatTableDataSource(response.response);
          this.dataSource['paginator'] = this.paginator;
          this.dataSource.sort = this.sort;
        }, error => {
          this.notificationService.toggleSnackBar('Something went wrong');
        })
    });

  }

}
