import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobsListingComponent } from './jobs-listing/jobs-listing.component';
import { MatModuleModule } from '../../mat-module.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    JobsRoutingModule,
    MatModuleModule,
    FormsModule
  ],
  declarations: [JobsListingComponent]
})
export class JobsModule { }
