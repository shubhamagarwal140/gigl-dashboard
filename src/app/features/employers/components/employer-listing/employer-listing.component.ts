import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../../environments/environment';
import { CommonService } from '../../../../shared/services/common.service';
import { NotificationService } from '../../../../shared/services/notification.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employer-listing',
  templateUrl: './employer-listing.component.html',
  styleUrls: ['./employer-listing.component.scss']
})

export class EmployerListingComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private commonService: CommonService,
    private notificationService: NotificationService,
    public dialog: MatDialog,
    private router: ActivatedRoute
  ) { }
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['name', 'email', 'mobile', 'action'];
  dataSource;
  singleData = {
    profileImage: '',
    isVerified: '',
    firstName: '',
    lastName: '',
    mobile: '',
    dob: '',
    jobCount: '',
    companyLogo: '',
    email: '',
    isBlocked: '',
    address: []
  };
  envtmnt = environment;
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  toggleBlocking(data){
    this.commonService.post('admin/employers/toggleBlocking', data)
      .subscribe(response => {
        console.log(response);
        if(response && response['message']){
          this.dataSource.filteredData.forEach(elem => {
            if(elem._id == data._id)
              elem.isBlocked = data.isBlocked;
          });

          this.notificationService.toggleSnackBar('Status changed Successfully');
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
  }

  getSingleUserData(empId){
    this.dataSource.filteredData.forEach(elem => {
      if(elem._id == empId)
        this.singleData = elem;
    });
  }

  ngOnInit() {
    this.router.paramMap
      .subscribe(resp => {
        var empLink = `${environment.baseUrl}admin/employers`;
        if(resp.get('empID')){
          var empId = resp.get('empID');
          empLink = `${environment.baseUrl}admin/employers/${empId}`;
        }
        
        this.http.get(empLink)
          .subscribe((response: any) => {
            console.log('response:-- ', response.response);
            this.dataSource  =  new MatTableDataSource(response.response);
            this.dataSource['paginator'] = this.paginator;
            this.dataSource.sort = this.sort;
          }, error => {
            this.notificationService.toggleSnackBar('Something went wrong');
          })
      })

  }

}