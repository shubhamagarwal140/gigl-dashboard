import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployerListingComponent } from './components/employer-listing/employer-listing.component';

const routes: Routes = [
  {
    path: '',
    component: EmployerListingComponent
  },
  {
    path: ':empID',
    component: EmployerListingComponent
  },
  // {
  //   path: 'edit/:empID',
  //   component: EditProfileComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployersRoutingModule { }
