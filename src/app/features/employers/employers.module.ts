import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployersRoutingModule } from './employers-routing.module';
import { EmployerListingComponent } from './components/employer-listing/employer-listing.component';
import { MatModuleModule } from '../../mat-module.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    EmployersRoutingModule,
    MatModuleModule,
    FormsModule
  ],
  declarations: [
    EmployerListingComponent
  ]
})
export class EmployersModule { }
