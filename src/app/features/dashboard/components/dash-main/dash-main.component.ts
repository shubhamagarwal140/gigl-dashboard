import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../../../shared/services/dashboard.service';

@Component({
  selector: 'app-dash-main',
  templateUrl: './dash-main.component.html',
  styleUrls: ['./dash-main.component.scss']
})
export class DashMainComponent implements OnInit {

  constructor(
    private dashboardService: DashboardService
  ) { }

  dataCount;

  ngOnInit() {
    this.dashboardService.dataCount()
      .subscribe(response => {
        console.log('count ', response);
        this.dataCount = response['response'];
      })
  }

}
