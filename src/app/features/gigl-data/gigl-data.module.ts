import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GiglDataRoutingModule } from './gigl-data-routing.module';
import { MatModuleModule } from '../../mat-module.module';
import { FormsModule } from '@angular/forms';

import { SkillsListingComponent } from './components/skills-listing/skills-listing.component';
import { QuestionListingComponent } from './components/question-listing/question-listing.component';

@NgModule({
  imports: [
    CommonModule,
    GiglDataRoutingModule,
    MatModuleModule,
    FormsModule
  ],
  declarations: [SkillsListingComponent, QuestionListingComponent]
})
export class GiglDataModule { }
