import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { environment } from '../../../../../environments/environment';
import { CommonService } from '../../../../shared/services/common.service';
import { NotificationService } from '../../../../shared/services/notification.service';

@Component({
  selector: 'app-question-listing',
  templateUrl: './question-listing.component.html',
  styleUrls: ['./question-listing.component.scss']
})
export class QuestionListingComponent implements OnInit {

 
  constructor(
    private commonService: CommonService,
    private notificationService: NotificationService,
    public dialog: MatDialog,
  ) { }
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['Title', 'action'];
  dataSource;
  envtmnt = environment;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  removeSkill(skill_id){
    var check = window.confirm('Are you sure you waana remove?');
    if(check){
      this.commonService.put('admin/data/removeQuestion/' + skill_id, {})
      .subscribe(response => {
        if(response && response['response']){
          // var i =0;
          // this.dataSource.filteredData.forEach(elem => {
          //   if(elem._id == skill_id){
          //     this.dataSource.filteredData.splice(i, 1);
          //     i++;
          //   }
          // })
          this.notificationService.toggleSnackBar('Skill Removed Successfully');
          window.location.href = window.location.href;
        }
        else this.notificationService.toggleSnackBar();
      },  error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      })
    }
  }

  addQuestion(value){
    console.log(value);
    if(value.question){
      this.commonService.post('admin/data/addQuestion', value)
      .subscribe(response => {
        if(response && response['response']){
          this.notificationService.toggleSnackBar('Question added successfully');
          setTimeout(() => {
            window.location.href = window.location.href;
          }, 1000)
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
    }
  }

  editQuestion(question){
    console.log(question); 
    var check = prompt( 'Enter updated question', question.question);
    console.log(check)
    if(check !== null){
      let value = { question : check, _id: question._id };
      this.commonService.put('admin/data/updateQuestion', value)
      .subscribe(response => {
        if(response && response['response']){
          this.notificationService.toggleSnackBar('Question updated successfully');
          setTimeout(() => {
            window.location.href = window.location.href;
          }, 1000)
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
    }
  }

  toggleBlocking(data){
    if(data.status == 2)
      data.status = 1;
    else data.status = 2;
    this.commonService.put('admin/data/questionBlocking', data)
      .subscribe(response => {
        if(response && response['response']){
          this.dataSource.filteredData.forEach(elem => {
            if(elem._id == data._id)
              elem.status = data.status;
          });

          this.notificationService.toggleSnackBar('Status changed Successfully');
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
  }

  ngOnInit() {
    // this.notificationService.toggleSnackBar('Loading....');
    this.commonService.get('admin/data/questionList')
      .subscribe((response: any) => {
        console.log('response:-- ', response.response);
        this.dataSource  =  new MatTableDataSource(response.response);
        this.dataSource['paginator'] = this.paginator;
        this.dataSource.sort = this.sort;
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      })

  }

}