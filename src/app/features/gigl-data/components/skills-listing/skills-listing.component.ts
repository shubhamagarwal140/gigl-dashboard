import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
// import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../../environments/environment';
import { CommonService } from '../../../../shared/services/common.service';
import { NotificationService } from '../../../../shared/services/notification.service';
// import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-skills-listing',
  templateUrl: './skills-listing.component.html',
  styleUrls: ['./skills-listing.component.scss']
})
export class SkillsListingComponent implements OnInit {

  constructor(
    // private http: HttpClient,
    private commonService: CommonService,
    private notificationService: NotificationService,
    public dialog: MatDialog,
    // private router: ActivatedRoute
  ) { }
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['Title', 'action'];
  dataSource;
  envtmnt = environment;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  removeSkill(skill_id){
    var check = window.confirm('Are you sure you waana remove?');
    if(check){
      this.commonService.put('admin/data/removeSkill/' + skill_id, {})
      .subscribe(response => {
        if(response && response['response']){
          // var i =0;
          // this.dataSource.filteredData.forEach(elem => {
          //   if(elem._id == skill_id){
          //     this.dataSource.filteredData.splice(i, 1);
          //     i++;
          //   }
          // })
          this.notificationService.toggleSnackBar('Skill Removed Successfully');
          setTimeout(() => {
            window.location.href = window.location.href;
          }, 1000)
        }
        else this.notificationService.toggleSnackBar();
      },  error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      })
    }
  }

  addSkill(value){
    console.log(value);
    if(value.skill){
      this.commonService.post('admin/data/addSkill', value)
      .subscribe(response => {
        if(response && response['response']){
          this.notificationService.toggleSnackBar('Skill added successfully');
          setTimeout(() => {
            window.location.href = window.location.href;
          }, 1000)
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
    }
  }

  editSkill(skill){
    console.log(skill); 
    var check = prompt( 'Enter updated skill', skill.skill);
    console.log(check)
    if(check !== null){
      let value = { skill : check, _id: skill._id };
      this.commonService.put('admin/data/updateSkill', value)
      .subscribe(response => {
        if(response && response['response']){
          this.notificationService.toggleSnackBar('Skill updated successfully');
          setTimeout(() => {
            window.location.href = window.location.href;
          }, 1000)
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
    }
  }

  toggleBlocking(data){
    if(data.status == 2)
      data.status = 1;
    else data.status = 2;
    this.commonService.put('admin/data/skillBlocking', data)
      .subscribe(response => {
        if(response && response['response']){
          this.dataSource.filteredData.forEach(elem => {
            if(elem._id == data._id)
              elem.status = data.status;
          });

          this.notificationService.toggleSnackBar('Status changed Successfully');
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      });
  }

  ngOnInit() {
    // this.notificationService.toggleSnackBar('Loading....');
    this.commonService.get('admin/data/skillList')
      .subscribe((response: any) => {
        console.log('response:-- ', response.response);
        if(response && response.response){
          this.dataSource  =  new MatTableDataSource(response.response);
          this.dataSource['paginator'] = this.paginator;
          this.dataSource.sort = this.sort;
        }
        else this.notificationService.toggleSnackBar('Something went wrong');
      }, error => {
        this.notificationService.toggleSnackBar('Something went wrong');
      })

  }

}