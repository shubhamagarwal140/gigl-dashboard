import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsListingComponent } from './skills-listing.component';

describe('SkillsListingComponent', () => {
  let component: SkillsListingComponent;
  let fixture: ComponentFixture<SkillsListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillsListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
