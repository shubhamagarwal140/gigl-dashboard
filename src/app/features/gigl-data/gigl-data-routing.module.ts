import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillsListingComponent } from './components/skills-listing/skills-listing.component';
import { QuestionListingComponent } from './components/question-listing/question-listing.component';

const routes: Routes = [
  {
    path: 'skills',
    component: SkillsListingComponent
  },
  {
    path: 'questions',
    component: QuestionListingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiglDataRoutingModule { }
