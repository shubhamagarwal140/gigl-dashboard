import { NgModule } from '@angular/core';

import {MatTableModule, 
  MatPaginatorModule,
  MatFormFieldModule,
  MatInputModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatSelectModule,
  MatDialogModule,
  MatButtonModule
} from '@angular/material';

@NgModule({
  exports: [
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule, 
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatButtonModule
  ]
})
export class MatModuleModule { }
