import { Component, OnInit } from '@angular/core';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { environment } from "../../../environments/environment";
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpClient,
    private route: ActivatedRoute,
    private notificationService: NotificationService
  ) { }

  forgotPass = 0; // 1= enter email 2= change pass
  errorMessage = '';
  resetToken;
  buttonLoading  = false; 

  login(data){
    this.errorMessage = '';
    
    this.http.post( environment.baseUrl + 'admin/login', data)
      .subscribe((response: any) => {
        if(response && response.token) this.createToken(response.token);
        else  this.errorMessage = 'Something went wrong';
      },
    error => {
      console.log('error ', error);
      if(error && error.error['message']) this.errorMessage = error.error['message'];
      else this.errorMessage = 'Something went wrong';
    });

  }

  createToken(token){
    localStorage.setItem('token', token);
    let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    this.router.navigate([returnUrl || '/dashboard']);
    this.notificationService.toggleSnackBar('Logged in Successfully');
  }

  forgotPassword(data){     
    this.buttonLoading = true;
    this.http.post( environment.baseUrl + 'admin/forgotPassword', data)
      .subscribe((response: any) => {
        this.buttonLoading = false;
        if(response && response['message'] ) this.errorMessage = response['message'];
      },
    error => {
        this.buttonLoading = false;
        console.log('error ', error);
    });
  }

  resetPass(value){
    if(value.password !== value.Cpassword)
      this.errorMessage = 'Confirm Password does not match';
    else{
      value.token = this.resetToken;
      this.http.post( environment.baseUrl + 'admin/resetPass', value)
        .subscribe((response: any) => {
          if(response && response['message'] ) this.errorMessage = response['message'];
        },
      error => {
        console.log('error ', error);
      });
    }
  }

  ngOnInit() {
    if(localStorage.getItem('token'))
      this.router.navigate(['/dashboard']);
    else{
      this.route.paramMap
      .subscribe(response => {
        var token = response.get('token');
        if(token){
          this.resetToken = token;
          this.forgotPass = 2;
        }
      })
    }
  }

}
