import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    public authService: AuthService,
    public commonService: CommonService
  ) { }

  adminData;
  environment = environment;
  multiSideItem: any={
    id: 0,
    status: false
  }

  getAdminData(adminId){
    this.commonService.get(`admin/profile/${adminId}`)
      .subscribe(response => {
        console.log('response-- ', response);
        this.adminData = response['response'];
      })
  }

  ngOnInit() {
    if(this.commonService.decodeToken)
      this.getAdminData(this.commonService.decodeToken._id);
  }

  sidebarCollapsed() {
    document.getElementById("bodySidebar").classList.toggle("layout-sidebar-collapsed");
    document.getElementById("sidebarWrap").classList.toggle("collapsed");
  }

  toggleMultiSidebar(){

  }
}
