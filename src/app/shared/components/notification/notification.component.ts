import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  constructor(
    private notificationService: NotificationService,
    public snackBar: MatSnackBar
  ) { }

  // snackbar = { 
  //   status: false,
  //   type: 'success',
  //   message: 'Loading..'
  // };

  // toggleSnackbar(data){
  //   console.log('snackbar2 ', data);
    
  //   this.snackbar.type = data.type;
  //   this.snackbar.message = data.message;
  //   this.snackbar.status = true;
    
  //   if(data.autoclose){
  //     setTimeout( () => {
  //       console.log('timeout', this.snackbar.status);
        
  //       this.snackbar.status = false;
  //     }, 3000);
  //   }
  // }

  ngSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 3000,
    });
  }

  ngOnInit() {
    this.notificationService.snackbar$
      .subscribe ( response => {
        // console.log('snackbar ', response);
        // this.toggleSnackbar(response);
        this.ngSnackBar(response)
      });
  }

}
