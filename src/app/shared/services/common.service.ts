import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { JwtHelper } from "angular2-jwt";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private http: HttpClient
  ) { }

  post(url, params){
    return this.http.post(environment.baseUrl + url, params);
  }

  get(url){
    return this.http.get(environment.baseUrl + url);
  }

  put(url, params){
    return this.http.put(environment.baseUrl + url, params);
  }

  get decodeToken(){
    var token = localStorage.getItem('token');    
    var jwthelper = new JwtHelper();
    
    if(token){
      var data = jwthelper.decodeToken(token);
      // console.log("jwthelper:- ",data);
      return data;
    }
    return null;
  }

}
