import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
// import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployerService {

  constructor(
    private http: HttpClient
  ) { }

  getEmployerData(empID){
    return this.http.get(environment.baseUrl + `admin/employers/${empID}`);
  }

}
