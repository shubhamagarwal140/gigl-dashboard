import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor() { }

  snackbar = new Subject<any>();

  snackbar$ = this.snackbar.asObservable();

  toggleSnackBar(message = 'Something Went Wrong'){
    this.snackbar.next(message);
  }

}
