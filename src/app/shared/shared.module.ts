import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

import { AuthService } from './services/auth.service';
import { CommonService } from './services/common.service';
import { AuthGuard } from './services/auth-guard.service';
import { NotificationService } from './services/notification.service';
import { NotificationComponent } from './components/notification/notification.component';
import { DashboardService } from './services/dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule    
  ],
  providers: [
    AuthService,
    CommonService,
    AuthGuard,
    NotificationService,
    DashboardService
  ],
  exports: [
    HeaderComponent, 
    FooterComponent,
    NotificationComponent
  ],
  declarations: [HeaderComponent, FooterComponent, NotificationComponent]
})
export class SharedModule { }
