import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './layouts/main/main.component';
import { NotFoundComponent } from './layouts/not-found/not-found.component';
import { LoginComponent } from './layouts/login/login.component';
import { AuthGuard } from './shared/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'password-reset/:token',
    component: LoginComponent
  },
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './features/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'employer',
        loadChildren: './features/employers/employers.module#EmployersModule'
      },
      {
        path: 'user',
        loadChildren: './features/jobseekers/jobseekers.module#JobseekersModule'
      },
      {
        path: 'admin',
        loadChildren: './features/admin/admin.module#AdminModule'
      },
      {
        path: 'jobs',
        loadChildren: './features/jobs/jobs.module#JobsModule'
      },
      {
        path: 'employer-jobs/:empId',
        loadChildren: './features/jobs/jobs.module#JobsModule'
      },
      {
        path: 'data',
        loadChildren: './features/gigl-data/gigl-data.module#GiglDataModule'
      }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
